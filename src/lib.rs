#![forbid(missing_docs)]
#![forbid(unsafe_code)]

//! A crate for reading/writing from/to byte buffers.
//!
//! This crate uses big-endian for all numbers.
//!
//! # Usage Example
//!
//! ```rust
//! use {
//!     pigeon::{
//!         Writer,
//!         Reader,
//!         Target,
//!         Unpack,
//!         Pack,
//!         WriteResult,
//!         ReadResult,
//!         ReadError,
//!         U3,
//!         U5,
//!     },
//! };
//!
//! #[derive(Debug, PartialEq)]
//! pub struct Animal {
//!     pub position: (u8, u8, u8),
//!     pub fluffy: bool,
//!     pub name: String,
//!     pub weight: f32,
//! }
//!
//! impl Pack for Animal {
//!     fn pack<T: Target>(&self, writer: &mut Writer<T>) -> WriteResult<()> {
//!         writer.write((U3(self.position.0), U3(self.position.1), U3(self.position.2)))?;
//!         writer.write(self.fluffy)?;
//!         let name_bytes = self.name.as_bytes();
//!         writer.write(U5(name_bytes.len() as u8))?;
//!         writer.write_bytes(name_bytes)?;
//!         writer.write(self.weight)?;
//!         Ok(())
//!     }
//! }
//!
//! impl<'a> Unpack<'a> for Animal {
//!     fn unpack(reader: &mut Reader<'a>) -> ReadResult<Self> {
//!         let (U3(x), U3(y), U3(z)) = reader.read()?;
//!         let position = (x, y, z);
//!         let fluffy = reader.read()?;
//!         let U5(name_len_u8) = reader.read()?;
//!         let name_len = name_len_u8 as usize;
//!         let name_bytes_unaligned = reader.read_bytes(name_len)?;
//!         let mut name_bytes = [0; 256];
//!         name_bytes_unaligned.copy_to_slice(&mut name_bytes[0..name_len]);
//!         let name = String::from_utf8(name_bytes[0..name_len].to_vec()).map_err(|_| ReadError::UnexpectedData)?;
//!         let weight = reader.read()?;
//!         Ok(Animal {
//!             position,
//!             fluffy,
//!             name: name.to_owned(),
//!             weight,
//!         })
//!     }
//! }
//!
//! let kitty = Animal {
//!     position: (3, 2, 7),
//!     fluffy: true,
//!     name: "Joseph Joestar".to_owned(),
//!     weight: 12.,
//! };
//!
//! let mut buf = [0; 128];
//!
//! let mut writer = Writer::new(&mut buf[..]);
//!
//! writer.write(&kitty).unwrap();
//!
//! let len = writer.finish().unwrap();
//!
//! let mut reader = Reader::new(&buf[..len]);
//!
//! let kitty_: Animal = reader.read().unwrap();
//!
//! assert_eq!(kitty, kitty_);
//! ```

mod adapters;
mod impls;
mod pack;
mod unaligned_slice;
mod unpack;

pub use {
    adapters::{U1, U2, U3, U4, U5, U6, U7},
    pack::{pack_buffer, Error as WriteError, Pack, Result as WriteResult, Target, Writer},
    unaligned_slice::{Iter, UnalignedSlice},
    unpack::{unpack_buffer, Error as ReadError, Reader, Result as ReadResult, Unpack},
};
