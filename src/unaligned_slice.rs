use core::cmp::{Eq, PartialEq};

/// A byte slice which is not aligned, but does contain a whole number of bytes.
#[derive(Debug, Clone, Copy)]
pub struct UnalignedSlice<'a> {
    bytes: &'a [u8],
    bit_offset: u32,
}

impl<'a> Eq for UnalignedSlice<'a> {}

impl<'a> PartialEq for UnalignedSlice<'a> {
    fn eq(&self, other: &UnalignedSlice<'a>) -> bool {
        let mut iter_self = self.iter();
        let mut iter_other = other.iter();
        loop {
            let byte_self = iter_self.next();
            let byte_other = iter_other.next();
            if byte_self != byte_other {
                return false;
            }
            if byte_self.is_none() {
                return true;
            }
        }
    }
}

impl<'a> PartialEq<&'a [u8]> for UnalignedSlice<'a> {
    fn eq(&self, other: &&'a [u8]) -> bool {
        self.eq(&UnalignedSlice::new(other, 0))
    }
}

impl<'a> UnalignedSlice<'a> {
    /// Create a new `UnalignedSlice` from a byte slice and a bit offset.
    pub fn new(bytes: &'a [u8], bit_offset: u32) -> Self {
        Self { bytes, bit_offset }
    }

    /// Get the length of this unaligned slice.
    pub fn len(&self) -> usize {
        let extra_byte = if self.bit_offset == 0 { 0 } else { 1 };
        self.bytes.len() - extra_byte
    }

    /// Copy the contents to an aligned slice.
    pub fn copy_to_slice(&self, buf: &mut [u8]) {
        assert_eq!(self.len(), buf.len());
        for (idx, byte) in self.iter().enumerate() {
            buf[idx] = byte;
        }
    }

    /// Create a `Vec` and write this data into it.
    pub fn to_vec(&self) -> Vec<u8> {
        let mut buf = vec![0; self.len()];
        self.copy_to_slice(&mut buf[..]);
        buf
    }

    /// Get an iterator over the bytes.
    pub fn iter(&self) -> Iter<'a> {
        let mut iter = self.bytes.iter();
        let (mask_first, mask_second, last) = if self.bit_offset == 0 {
            (0, 0xFF, 0)
        } else {
            let mask_first = 0xFF >> (8 - self.bit_offset);
            let mask_second = 0xFF << self.bit_offset;
            if let Some(&byte) = iter.next() {
                (mask_first, mask_second, byte)
            } else {
                (mask_first, mask_second, 0)
            }
        };
        Iter {
            iter,
            bit_offset: self.bit_offset,
            mask_first,
            mask_second,
            last,
        }
    }
}

/// An iterator over the bytes in an `UnalignedSlice`.
pub struct Iter<'a> {
    iter: core::slice::Iter<'a, u8>,
    bit_offset: u32,
    mask_first: u8,
    mask_second: u8,
    last: u8,
}

impl<'a> Iterator for Iter<'a> {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(&current) = self.iter.next() {
            if self.bit_offset == 0 {
                Some(current)
            } else {
                let fst = self.last & self.mask_first;
                let snd = current & self.mask_second;
                let byte = (fst | snd).rotate_right(self.bit_offset);
                self.last = current;
                Some(byte)
            }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_unaligned_slice() {
        let data = [0x12, 0x34, 0x56, 0x78];
        let slice_a = UnalignedSlice::new(&data[1..3], 4);
        assert_eq!(slice_a.len(), 1);
        assert_eq!(slice_a, &[0x45][..]);
        let slice_b = UnalignedSlice::new(&data[1..4], 4);
        assert_eq!(slice_b.len(), 2);
        assert_eq!(slice_b, &[0x45, 0x67][..]);
    }

    #[test]
    fn test_unaligned_slice_iter() {
        let data = [0x12, 0x34, 0x56, 0x78];
        let slice = UnalignedSlice::new(&data[0..3], 4);
        let mut iter = slice.iter();
        assert_eq!(iter.next(), Some(0x23));
        assert_eq!(iter.next(), Some(0x45));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_unaliged_slice_copy_to() {
        let data = [0x12, 0x34, 0x56, 0x78];
        let slice = UnalignedSlice::new(&data[0..3], 4);
        let mut buf = [0; 2];
        slice.copy_to_slice(&mut buf[..]);
        assert_eq!(&buf[..2], &[0x23, 0x45]);
    }
}
