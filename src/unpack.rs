use crate::unaligned_slice::UnalignedSlice;

/// An error which can be emitted when a read fails.
#[derive(Debug, PartialEq)]
pub enum Error {
    /// Reached the end of the buffer.
    UnexpectedEof,
    /// Reached data that wasn't expected.
    UnexpectedData,
    /// An unknown other error has been encountered.
    Other,
}

impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match *self {
            Error::UnexpectedEof => write!(f, "unexpected end of the buffer"),
            Error::UnexpectedData => write!(f, "unexpected data"),
            Error::Other => write!(f, "other"),
        }
    }
}

impl std::error::Error for Error {}

/// A type alias for a `Result` that returns an `Error`.
pub type Result<T> = core::result::Result<T, Error>;

/// Types which can get read from a `Reader`.
pub trait Unpack<'a>
where
    Self: Sized + 'a,
{
    /// Read an item of this type from the `Reader`.
    fn unpack(reader: &mut Reader<'a>) -> Result<Self>;
}

/// Read a type which implement `Unpack` from the buffer.
///
/// This is a short-hand for creating a `Reader` and then calling `read` on that.
///
/// # Examples
///
/// ```rust
/// let buf = [12, 34, 56, 78];
///
/// let num: u32 = pigeon::unpack_buffer(&buf[..]).unwrap();
///
/// assert_eq!(num, 203569230);
/// ```
pub fn unpack_buffer<'a, U: Unpack<'a>>(buffer: &'a [u8]) -> Result<U> {
    let mut reader = Reader::new(buffer);
    reader.read()
}

/// A type that can be used to read from a buffer.
#[derive(Debug)]
pub struct Reader<'a> {
    idx: usize,
    bit_remain: u8,
    bit_buffer: u16,
    buf: &'a [u8],
}

impl<'a> Reader<'a> {
    /// Create a new `Reader` from a buffer.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0, 1, 2, 3];
    /// let mut reader = Reader::new(&buf[..]);
    /// let a = reader.read::<u8>().unwrap();
    /// let b = reader.read::<u16>().unwrap();
    /// let c = reader.read::<u8>().unwrap();
    /// assert_eq!(a, 0);
    /// assert_eq!(b, 258);
    /// assert_eq!(c, 3);
    /// assert!(reader.read::<u8>().is_err());
    /// ```
    pub fn new(buf: &'a [u8]) -> Reader<'a> {
        Reader {
            idx: 0,
            bit_remain: 0,
            bit_buffer: 0,
            buf,
        }
    }

    /// Get the position of the `Reader` in the buffer.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0; 16];
    /// let mut reader = Reader::new(&buf[..]);
    /// assert_eq!(reader.position(), 0);
    /// reader.read_bytes(2).unwrap();
    /// assert_eq!(reader.position(), 2);
    /// reader.read_bytes(5).unwrap();
    /// assert_eq!(reader.position(), 7);
    /// reader.read::<u32>().unwrap();
    /// assert_eq!(reader.position(), 11);
    /// reader.read::<u8>().unwrap();
    /// assert_eq!(reader.position(), 12);
    /// reader.read::<u32>().unwrap();
    /// assert_eq!(reader.position(), 16);
    /// ```
    pub fn position(&self) -> usize {
        self.idx
    }

    /// Skip bits until the reader is aligned to a byte boundary.
    ///
    /// # Position
    ///
    /// This will advance the position by 1 if the reader is currently unaligned.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0xF0, 0x0F, 0xFF];
    /// let mut reader = Reader::new(&buf[..]);
    /// reader.skip_align();
    /// assert_eq!(reader.read_u8_bits(4).unwrap(), 0x0F);
    /// reader.skip_align();
    /// assert_eq!(reader.read_u8().unwrap(), 0x0F);
    /// reader.skip_align();
    /// assert_eq!(reader.read_u8_bits(4).unwrap(), 0x0F);
    /// reader.skip_align();
    /// assert!(reader.read_u8_bits(2).is_err());
    /// ```
    pub fn skip_align(&mut self) {
        self.bit_remain = 0;
        self.bit_buffer = 0;
    }

    /// Try to read a byte from the buffer.
    ///
    /// # Position
    ///
    /// This will only advance the position if the read succeeds.
    ///
    /// # Returns
    ///
    /// On success, this returns `Ok` with a slice of the bytes read.
    /// If there are not enough bytes remaining in the buffer, this returns
    /// `Err(Error::UnexpectedEof)`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0x42, 0xFF, 0x0F];
    /// let mut reader = Reader::new(&buf[..]);
    /// assert_eq!(reader.read_u8().unwrap(), 0x42);
    /// assert_eq!(reader.read_u8_bits(4).unwrap(), 0x0F);
    /// assert_eq!(reader.read_u8().unwrap(), 0xF0);
    /// assert!(reader.read_u8().is_err());
    /// ```
    pub fn read_u8(&mut self) -> Result<u8> {
        if self.bit_remain < 8 {
            let buf_len = self.buf.len();
            if self.idx + 1 <= buf_len {
                let idx = self.idx;
                let byte = self.buf[idx];
                let shifted_bit_buffer = u16::wrapping_shl(self.bit_buffer, 8);
                self.bit_buffer = shifted_bit_buffer | byte as u16;
                self.bit_remain += 8;
                self.idx += 1;
            } else {
                return Err(Error::UnexpectedEof);
            }
        }
        let ret = u16::wrapping_shr(self.bit_buffer, (self.bit_remain - 8) as u32);
        self.bit_remain -= 8;
        Ok((ret & 0xFF) as u8)
    }

    /// Try to read `bits_count` bits from the buffer, where `bits_count` is assumed to be less
    /// than or equal to 8.
    ///
    /// # Position
    ///
    /// This will only advance the position if the read succeeds.
    ///
    /// # Returns
    ///
    /// On success, this returns `Ok` with a `u8`.
    /// If there are not enough bytes remaining in the buffer, this returns
    /// `Err(Error::UnexpectedEof)`.
    ///
    /// # Panics
    ///
    /// This panics if `bits_count` is less than 1 or greater than 8.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0xF0, 0x0F, 0x0F];
    /// let mut reader = Reader::new(&buf[..]);
    /// assert_eq!(reader.read_u8_bits(4).unwrap(), 0x0F);
    /// assert_eq!(reader.read_u8_bits(8).unwrap(), 0x00);
    /// assert_eq!(reader.read_u8_bits(4).unwrap(), 0x0F);
    /// assert_eq!(reader.read_u8_bits(8).unwrap(), 0x0F);
    /// assert!(reader.read_u8_bits(2).is_err());
    /// ```
    pub fn read_u8_bits(&mut self, bits_count: u8) -> Result<u8> {
        assert!(bits_count <= 8);
        assert!(bits_count > 0);
        if bits_count > self.bit_remain {
            let buf_len = self.buf.len();
            if self.idx + 1 <= buf_len {
                let idx = self.idx;
                let byte = self.buf[idx];
                let shifted_bit_buffer = self.bit_buffer << 8;
                self.bit_buffer = shifted_bit_buffer | byte as u16;
                self.bit_remain += 8;
                self.idx += 1;
            } else {
                return Err(Error::UnexpectedEof);
            }
        }
        let ret = self.bit_buffer >> (self.bit_remain - bits_count);
        self.bit_remain -= bits_count;
        let ret_mask = 0x00FF >> (8 - bits_count);
        Ok((ret & ret_mask) as u8)
    }

    /// Try to read `len` bytes from the buffer.
    ///
    /// # Position
    ///
    /// This will only advance the position if the read succeeds.
    ///
    /// # Returns
    ///
    /// On success, this returns `Ok` with a slice of the bytes read.
    /// If there are not enough bytes remaining in the buffer, this returns
    /// `Err(Error::UnexpectedEof)`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0, 1, 2, 3, 4, 5];
    /// let mut reader = Reader::new(&buf[..]);
    /// let bytes_a = reader.read_bytes(3).unwrap();
    /// assert_eq!(bytes_a, &[0, 1, 2][..]);
    /// let bytes_b = reader.read_bytes(2).unwrap();
    /// assert_eq!(bytes_b, &[3, 4][..]);
    /// assert!(reader.read_bytes(2).is_err());
    /// ```
    pub fn read_bytes(&mut self, len: usize) -> Result<UnalignedSlice<'a>> {
        let buf_len = self.buf.len();
        if self.idx + len <= buf_len {
            let idx = self.idx;
            self.idx += len;
            if self.bit_remain == 0 {
                let slice = &self.buf[idx..idx + len];
                Ok(UnalignedSlice::new(slice, 0))
            } else {
                let slice = &self.buf[idx - 1..idx + len];
                self.bit_buffer = self.buf[idx + len - 1] as u16;
                Ok(UnalignedSlice::new(slice, self.bit_remain as u32))
            }
        } else {
            Err(Error::UnexpectedEof)
        }
    }

    /// Try to read `buf.len()` bytes into `buf`.
    ///
    /// # Position
    ///
    /// This will only advance the position if the read succeeds.
    ///
    /// # Returns
    ///
    /// On success, this returns `Ok(())` and the buffer will be filled with the read data.
    /// If there are not enough bytes remaining in the buffer, this returns
    /// `Err(Error::UnexpectedEof)`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0, 1, 2, 3, 4, 5];
    /// let mut reader = Reader::new(&buf[..]);
    /// let mut target = [0; 4];
    /// reader.read_bytes_to(&mut target[..]).unwrap();
    /// assert_eq!(&target[..4], &buf[..4]);
    /// assert!(reader.read_bytes_to(&mut target[..]).is_err());
    /// ```
    pub fn read_bytes_to(&mut self, buf: &mut [u8]) -> Result<()> {
        let bytes = self.read_bytes(buf.len())?;
        bytes.copy_to_slice(buf);
        Ok(())
    }

    /// Try to read an instance of a type implementing `Unpack`.
    ///
    /// # Position
    ///
    /// This will advance the position only if all of the inner reads have succeeded.
    ///
    /// # Returns
    ///
    /// On success, this returns `Ok` with an instance of `U` inside it.
    /// On failure, this returns `Err`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0, 1, 2, 3, 4, 5, 6];
    /// let mut reader = Reader::new(&buf[..]);
    /// let a: u32 = reader.read().unwrap();
    /// assert_eq!(a, 66051);
    /// let b: u8 = reader.read().unwrap();
    /// assert_eq!(b, 4);
    /// assert!(reader.read::<u32>().is_err());
    /// let c: u16 = reader.read().unwrap();
    /// assert_eq!(c, 1286);
    /// assert!(reader.read::<u8>().is_err());
    /// ```
    pub fn read<U: Unpack<'a>>(&mut self) -> Result<U> {
        let last_idx = self.idx;
        match U::unpack(self) {
            Ok(ret) => Ok(ret),
            Err(err) => {
                self.idx = last_idx;
                Err(err)
            }
        }
    }

    /// Try to read an instance of a type implementing `Unpack`, and compare it to `expected`.
    ///
    /// # Position
    ///
    /// This will advance the position if the read succeeded but doesn't match the expected value,
    /// but in all other cases it will not advance the position.
    ///
    /// # Returns
    ///
    /// On success, returns `Ok(())`.
    /// When the read succeeds, but the values do not match, returns `Err(Error::UnexpectedData)`.
    /// On an underlying failure, returns `Err(_)`.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// #     ReadError,
    /// # };
    /// let buf = [0, 1, 2, 3, 4, 5, 5, 6];
    /// let mut reader = Reader::new(&buf[..]);
    /// reader.expect(66051u32).unwrap();
    /// reader.expect(4u8).unwrap();
    /// assert_eq!(reader.expect(4u8), Err(ReadError::UnexpectedData));
    /// assert_eq!(reader.expect(12345678u32), Err(ReadError::UnexpectedEof));
    /// reader.expect(1286u16).unwrap();
    /// ```
    pub fn expect<U: Unpack<'a> + PartialEq>(&mut self, expected: U) -> Result<()> {
        let data: U = self.read()?;
        if data == expected {
            Ok(())
        } else {
            Err(Error::UnexpectedData)
        }
    }

    /// Create a `Reader` on a buffer and pass it into a closure, returning what the closure
    /// returns.
    ///
    /// # Examples
    ///
    /// ```rust
    /// # use pigeon::{
    /// #     Reader,
    /// # };
    /// let buf = [0, 1, 2, 3, 4, 5, 6];
    /// let value = Reader::with(&buf[..], |reader| {
    ///     let a: u32 = reader.read()?;
    ///     let b: u16 = reader.read()?;
    ///     let c: u8 = reader.read()?;
    ///     Ok((a, b, c))
    /// }).unwrap();
    /// assert_eq!(value, (66051, 1029, 6));
    /// ```
    pub fn with<T>(buf: &'a [u8], cb: impl FnOnce(&mut Reader<'a>) -> Result<T>) -> Result<T> {
        let mut reader = Reader::new(buf);
        cb(&mut reader)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_reading_1_a() {
        let buf = [0x80, 0x40];
        let mut reader = Reader::new(&buf[..]);
        assert_eq!(reader.read::<bool>().unwrap(), true);
        assert_eq!(reader.bit_remain, 7);
        assert_eq!(reader.bit_buffer & 0x007F, 0x0000);
        assert_eq!(reader.read::<u8>().unwrap(), 0u8);
        assert_eq!(reader.bit_remain, 7);
        assert_eq!(reader.bit_buffer & 0x007F, 0x0040);
        assert_eq!(reader.read::<bool>().unwrap(), true);
        assert_eq!(reader.bit_remain, 6);
        assert_eq!(reader.bit_buffer & 0x003F, 0x0000);
    }
}
