use crate::{
    pack::{Pack, Result as WriteResult, Target, Writer},
    unpack::{Reader, Result as ReadResult, Unpack},
};

/// Adapter for reading/writing a single bit.
pub struct U1(pub bool);

impl Pack for U1 {
    fn pack<T: Target>(&self, writer: &mut Writer<T>) -> WriteResult<()> {
        let byte = if self.0 { 0xFF } else { 0x00 };
        writer.write_u8_bits(1, byte)
    }
}

impl<'a> Unpack<'a> for U1 {
    fn unpack(reader: &mut Reader<'a>) -> ReadResult<Self> {
        let byte = reader.read_u8_bits(1)?;
        Ok(U1(byte != 0))
    }
}

macro_rules! impl_for_sub_byte_bits_adapter {
    ($t:ty, $bits_count:expr) => {
        impl Pack for $t {
            fn pack<T: Target>(&self, writer: &mut Writer<T>) -> WriteResult<()> {
                writer.write_u8_bits($bits_count, self.0)
            }
        }

        impl<'a> Unpack<'a> for $t {
            fn unpack(reader: &mut Reader<'a>) -> ReadResult<Self> {
                let byte = reader.read_u8_bits($bits_count)?;
                Ok(Self(byte))
            }
        }
    };
}

macro_rules! impl_for_sub_byte_bits_adapter_multi {
    ($(($t:ty, $bits_count:expr)),*) => {
        $(impl_for_sub_byte_bits_adapter!($t, $bits_count);)*
    }
}

/// Adapter for reading/writing a 2-bit unsigned integer.
///
/// This will only read/write the last 2 least significant bits of the inner `u8`.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct U2(pub u8);

/// Adapter for reading/writing a 3-bit unsigned integer.
///
/// This will only read/write the last 3 least significant bits of the inner `u8`.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct U3(pub u8);

/// Adapter for reading/writing a 4-bit unsigned integer.
///
/// This will only read/write the last 4 least significant bits of the inner `u8`.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct U4(pub u8);

/// Adapter for reading/writing a 5-bit unsigned integer.
///
/// This will only read/write the last 5 least significant bits of the inner `u8`.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct U5(pub u8);

/// Adapter for reading/writing a 6-bit unsigned integer.
///
/// This will only read/write the last 6 least significant bits of the inner `u8`.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct U6(pub u8);

/// Adapter for reading/writing a 7-bit unsigned integer.
///
/// This will only read/write the last 7 least significant bits of the inner `u8`.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct U7(pub u8);

impl_for_sub_byte_bits_adapter_multi!((U2, 2), (U3, 3), (U4, 4), (U5, 5), (U6, 6), (U7, 7));

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_u2() {
        let mut buf = [0; 1];
        let mut writer = Writer::new(&mut buf[..]);
        writer.write(U2(2)).unwrap();
        let len = writer.finish().unwrap();
        assert_eq!(len, 1);
        let mut reader = Reader::new(&buf[..]);
        let U2(num) = reader.read().unwrap();
        assert_eq!(num, 2);
    }
}
